package main

import "fmt"

func scope() func() int {
	outerVar := 2
	f := func() int { return outerVar }
	return f
}

func main() {
	fmt.Printf("%T %v", scope()(), scope()())

}
