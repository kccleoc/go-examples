//函数闭包

// nextNum这个函数返回了一个匿名函数，这个匿名函数记住了nextNum中i+j的值，并改变了i,j的值，于是形成了一个闭包的用法

package main

import "fmt"

func nextNum() func() int {
	i, j := 1, 1
	// fmt.Println("once only")
	return func() int {
		// fmt.Println("multiple times")
		var tmp = i + j
		i, j = j, tmp
		return tmp
	}
}

//main函数中是对nextNum的调用，其主要是打出下一个斐波拉契数
func main() {
	nextNumFunc := nextNum()
	for i := 0; i < 10; i++ {
		fmt.Println(nextNumFunc())
	}
}
