package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {
	var flgEnv = flag.String("e", "GOPATH", "get the environmental variable in OS")
	flag.Parse()
	s := os.Getenv(*flgEnv) // check environmental var
	fmt.Printf("env var : %s is %s", *flgEnv, s)
}
