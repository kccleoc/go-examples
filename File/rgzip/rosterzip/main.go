package main

import (
	"compress/gzip"
	"context"
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"
)

func getHash(p string) (string, error) {
	f, err := ioutil.ReadFile(p)
	if err != nil {
		log.Printf("could not get hash of %s : %s\n", p, err)
		return "", err
	}
	h := sha256.New()
	h.Write(f)
	return fmt.Sprintf("%x", h.Sum(nil)), nil
}

// Start will start monitor the target file in a regular time
// interval as defined by this program
func zipStart(d time.Duration, xls string) {
	ctx, cancel := context.WithCancel(context.Background())

	defer func() { cancel(); log.Printf("ctx cancel %s", ctx.Err()) }()

	hash := func(hf string) string {
		hx, err := getHash(hf)
		if err != nil {
			return ""
		}
		return hx
	}
	// get hashes of csv and xls
	prevHash := map[string]string{xls: hash(xls)}
	// 	curHash := map[string]string{}

	// compare hash func returns xls or csv and time of change
	// when changes is found
	compare := func(s string) []string {
		chg := make([]string, 3)
		if t := hash(s); prevHash[s] != t {
			if t == "" {
				return chg
			}
			prevHash[s] = t
			chg[0] = s
			chg[1] = time.Now().Format("2006-01-02 15-04-05")

			return chg
		}
		return []string{"", ""}
	}

	// get updated hash at regular interval - control by user
	ticker := time.NewTicker(d)

	// continuous checking
	for {
		select {
		case <-ctx.Done():
			log.Printf("watchers stop signal sent : %s\n", ctx.Err())
			return
		case <-ticker.C:
			s := compare(xls)
			if s[0] == "" {
				log.Println("tracking...")
				continue
			}
			log.Println("changed: ", s)
			zipRoster(s)

		}
	}

}

func zipRoster(fileInfo []string) {

	// Open original file
	originalFile, err := os.Open(fileInfo[0])
	if err != nil {
		log.Fatal(err)
	}
	defer originalFile.Close()

	data, err := ioutil.ReadAll(originalFile)
	if err != nil {
		log.Fatal(err)
	}

	// Create .gz file to write to
	outputFile, err := os.Create(fmt.Sprintf("./archiveRoster/%s.zip", fileInfo[1]))
	if err != nil {
		log.Fatal(err)
	}

	// Create a gzip writer on top of file writer
	gzipWriter := gzip.NewWriter(outputFile)
	defer gzipWriter.Close()

	// write bytes to gzip
	_, err = gzipWriter.Write(data)
	if err != nil {
		log.Fatal(err)
	}

}

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println(`usage: rosterzip [file name] [duration in second]
			- Auto backup target file in ./archiveRoster folder
			- tested in linux environment
			`)
		}
	}()

	directory := "./archiveRoster/"
	if _, err := os.Stat(directory); os.IsNotExist(err) {
		os.MkdirAll(directory, os.ModePerm)
	}

	xls := os.Args[1]
	d, _ := strconv.Atoi(os.Args[2])

	dur := time.Duration(d) * time.Second
	zipStart(dur, xls)
}
