package main

import (
	"io"
	"log"
	"os"
	"strings"
)

func main() {

	dstfile, err := os.Create(os.Args[1])
	if err != nil {
		log.Fatalln("error createing file: ", err)
	}
	defer dstfile.Close()

	io.Copy(dstfile, strings.NewReader("haha"))
	dstfile.Write([]byte("sliceof byte"))
}
