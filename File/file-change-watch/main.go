package main

import (
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"github.com/naoina/toml"
)

const tomlPath = "setting.toml" //fixed location

type workFile struct {
	Path string
}

func tomlparse(wf *workFile, tomlFile string) *workFile {
	f, err := os.Open(tomlFile)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	if err := toml.NewDecoder(f).Decode(wf); err != nil {
		panic(err)
	}

	return wf
}

func getsha(filename string) ([]byte, error) {
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	h := sha256.New()
	h.Write(bs)
	return h.Sum(nil), nil
}

func watchFile(path string) error {

	//file hash
	hash0, err := getsha(path)
	if err != nil {
		fmt.Printf("could not get the hash %s ", err)
	}

	for {
		//new file stats
		fstat1, err := os.Stat(path)
		if err != nil {
			fmt.Printf("could not obtain subsequent file stat %s\n", err)
			return err
		}
		mt1 := fstat1.ModTime()

		// get new hash
		hash1, err := getsha(path)
		if err != nil {
			fmt.Printf("could not get the new hash %s ", err)
		}

		if string(hash0) != string(hash1) {
			fmt.Printf("Last Modified: %v ; ", mt1)
			break
		}

		time.Sleep(time.Second * 10)
	}

	return nil
}

func main() {
	w := new(workFile)
	tomlparse(w, tomlPath)

	doneChan := make(chan bool)
	for {
		// kick start the work by saying it is undone (i.e. false)
		go func() { doneChan <- false }()

		// main go ruoutine is blocking on receive
		if !<-doneChan {
			// launch a worker to track changes in file
			// when change detected, send true to main routine
			go func(c chan bool, fpath string) {
				fmt.Printf("start watching %s\n", fpath)
				defer func() { c <- true }()

				// channel is waiting for this event to be finished
				err := watchFile(fpath)
				if err != nil {
					fmt.Printf("could not initialise file watch %s\n", err)
					os.Exit(1)
				}

			}(doneChan, w.Path)

			// main routine blocked on waiting to receive
			// another signal after changes detected
			<-doneChan
		}
	}
}
