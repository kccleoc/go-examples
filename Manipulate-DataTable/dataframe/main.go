package main

import (
	"fmt"

	"github.com/kniren/gota/dataframe"
	"github.com/kniren/gota/series"
)

func main() {
	// 	jsonStr := `[{"COL.2":1,"COL.3":3},{"COL.1":5,"COL.2":2,"COL.3":2},{"COL.1":6,"COL.2":3,"COL.3":1}]`
	// 	df := dataframe.ReadJSON(strings.NewReader(jsonStr))
	// 	fmt.Println(df)

	// 	df2 := dataframe.New(
	// 		series.New([]string{"b", "a"}, series.String, "COL.1"),
	// 		series.New([]int{1, 2}, series.Int, "COL.2"),
	// 		series.New([]float64{3.0, 4.0}, series.Float, "COL.3"),
	// 	)
	// 	fmt.Println(df2)

	// 	csvStr := `
	// Country,Date,Age,Amount,Id
	// "United States",2012-02-01,50,112.1,01234
	// "United States",2012-02-01,32,321.31,54320
	// "United Kingdom",2012-02-01,17,18.2,12345
	// "United States",2012-02-01,32,321.31,54320
	// "United Kingdom",2012-02-01,NA,18.2,12345
	// "United States",2012-02-01,32,321.31,54320
	// "United States",2012-02-01,32,321.31,54320
	// Spain,2012-02-01,66,555.42,00241
	// `
	// 	df3 := dataframe.ReadCSV(strings.NewReader(csvStr))
	// 	fmt.Println(df3)

	// 	df4 := dataframe.LoadRecords(
	// 		[][]string{
	// 			[]string{"A", "B", "C", "D"},
	// 			[]string{"a", "4", "5.1", "true"},
	// 			[]string{"k", "5", "7.0", "true"},
	// 			[]string{"k", "4", "6.0", "true"},
	// 			[]string{"a", "2", "7.1", "false"},
	// 		},
	// 	)
	// 	sel1 := df4.Select([]int{0, 2})
	// 	sel2 := df4.Select([]string{"A", "C"})
	// 	fmt.Println(df4)
	// 	fmt.Println(sel1)
	// 	fmt.Println(sel2)

	// 	df5 := dataframe.LoadRecords(
	// 		[][]string{
	// 			[]string{"A", "B", "C", "D"},
	// 			[]string{"a", "4", "5.1", "true"},
	// 			[]string{"k", "5", "7.0", "true"},
	// 			[]string{"k", "4", "6.0", "true"},
	// 			[]string{"a", "2", "7.1", "false"},
	// 		},
	// 	)
	// 	sub := df5.Subset([]int{0, 2})
	// 	fmt.Println(sub)

	df6 := dataframe.LoadRecords(
		[][]string{
			[]string{"A", "B", "C", "D"},
			[]string{"a", "4", "5.1", "true"},
			[]string{"k", "5", "7.0", "true"},
			[]string{"k", "4", "6.0", "true"},
			[]string{"a", "2", "7.1", "false"},
		},
	)
	fil := df6.Filter(
		dataframe.F{
			Colname:    "A",
			Comparator: series.Eq,
			Comparando: "a",
		},
		dataframe.F{
			Colname:    "B",
			Comparator: series.Greater,
			Comparando: 4,
		},
	)
	fil2 := fil.Filter(
		dataframe.F{
			Colname:    "D",
			Comparator: series.Eq,
			Comparando: true,
		},
	)
	fmt.Println(fil)
	fmt.Println(fil2)
}
