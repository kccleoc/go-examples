package main

import (
	"net/http"
	"os"
)

func main() {
	dir, _ := os.Getwd()
	//fmt.Println(os.Getenv(":8090"))
	http.ListenAndServe(":80", http.FileServer(http.Dir(dir)))

}
