package main

import (
	"fmt"
	"io"
	"log"
	"net"
)

func main() {

	laddr, err := net.ResolveTCPAddr("tcp", ":2002")
	if err != nil {
		log.Fatalln("resove problem occured...")
	}
	ln, err := net.ListenTCP("tcp", laddr)
	if err != nil {
		log.Fatalln("Listening problem occured...")
	}
	fmt.Println("Listening on :", laddr)
	conn, err := ln.Accept()
	if err != nil {
		log.Fatalln("fail to establish connection")
	}
	buf := make([]byte, 1000)
	io.ReadFull(conn, buf)
	defer conn.Close()
	defer ln.Close()
	fmt.Print(string(buf))
}
