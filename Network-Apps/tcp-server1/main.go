package main

import (
	"bufio"
	"fmt"
	"io"
	"net"
)

func main() {

	ln, err := net.Listen("tcp", ":2001")
	if err != nil {
		panic("error listening...")
	}
	defer ln.Close()

	i := 1
	for {
		conn, err := ln.Accept()
		if err != nil {
			panic(err)
		}
		fmt.Println("Connected: ", conn.RemoteAddr(), "total: ", i)

		//for concurrent connection to happen
		go func() {
			/*Method 2 = bufio scanner*/
			scanner := bufio.NewScanner(conn)
			for scanner.Scan() {
				io.WriteString(conn, scanner.Text())
			}
			/**/

			/*  Method 1 = Echo server using stream of byte
			for {
				bs := make([]byte, 512)
				n, err := conn.Read(bs)
				if err != nil {
					break
				}
				_, err = conn.Write(bs[:n])
				if err != nil {
					break
				}
			}
			*/
			conn.Close()

		}()
		i++
	}
}
