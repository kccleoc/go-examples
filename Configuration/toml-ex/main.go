package main

import (
	"fmt"

	"github.com/kccleoc/go-examples/@Configuration/toml-ex/tomlconfig"
)

func main() {
	config, err := tomlconfig.LoadConfig("./ex1.toml")
	if err != nil {
		panic("error loading config file...")
	}
	fmt.Printf("%v", config.Keys["general"]["C-j"])
}
