package main

import (
	"context"
	"fmt"
	"os"
	"runtime/pprof"

	"time"
)

var pubChan = make(chan int)

func doWork(i int, ctx context.Context) {

	pubChan <- i
	fmt.Printf(" %v[sent] ", i)

	<-ctx.Done()
	fmt.Printf(" [**done** %v,%s]\n", i, ctx.Err())
	return

}

func main() {
	pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
	ctx, cancel := context.WithCancel(context.Background())

	for x := 1; x <= 5; x++ {
		go doWork(x, ctx)
	}
loop:
	for {
		select {
		case m := <-pubChan:
			fmt.Printf(" %v[REC'D] ", m)
			pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
		case <-time.After(3 * time.Second):
			fmt.Println("timed out")
			cancel()
			// 			close(pubChan)
			break loop
		}
	}
	close(pubChan)
	pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
}
