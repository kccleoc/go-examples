package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	s2 := rand.NewSource(time.Now().UnixNano())
	r2 := rand.New(s2)

	ch := make(chan string)
	go func() {
		select {

		case <-time.After(time.Second * time.Duration(r1.Intn(2))):
			ch <- "cake"
			fmt.Println("cake is sent")

		case <-time.After(time.Second * time.Duration(r2.Intn(2))):
			fmt.Println("Time Out!")
			close(ch)
		}
	}()
	<-time.After(time.Second * 3)
	b, ok := <-ch //if ch is closed, ok is false
	if ok {
		fmt.Printf("%v is received\n", b)
	} else {
		fmt.Printf("nothing received!\n")
	}
}
