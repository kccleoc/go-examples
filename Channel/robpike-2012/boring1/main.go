package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {

	Joe := boring("Joe!")
	Thomas := boring("Thomas!")
	for i := 0; i < 5; i++ {
		// take turn - Joe first then Thomas
		// When Joe is wating to
		// receive, it blocks and vice
		// versa
		fmt.Printf("%q\n", <-Joe)
		fmt.Printf("%q\n", <-Thomas)
	}
	fmt.Println("You're boring; I'm leaving.")
}

func boring(msg string) <-chan string {
	c := make(chan string)
	go func() {
		for i := 0; ; i++ {
			c <- fmt.Sprintf("%s %d", msg, i)
			time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
		}
	}()

	return c
}
