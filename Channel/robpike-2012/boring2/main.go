package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// whoever send to the channel will block the function
	// no more synchronization and keep one waiting
	// until
	// the others have been finish sending
	c := fanIn(boring("Joe!"), boring("Thmoas!"))

	for i := 0; i < 5; i++ {

		fmt.Printf("%q\n", <-c)
	}
	fmt.Println("You're boring; I'm leaving.")
}

func boring(msg string) <-chan string {
	c := make(chan string)
	go func() {
		for i := 0; ; i++ {
			c <- fmt.Sprintf("%s %d", msg, i)
			time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
		}
	}()

	return c
}

func fanIn(input1, input2 <-chan string) <-chan string {
	c := make(chan string)
	go func() {
		for {
			c <- <-input1
		}
	}()
	go func() {
		for {
			c <- <-input2
		}
	}()
	return c
}
