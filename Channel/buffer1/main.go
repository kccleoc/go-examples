package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const maxRandNum = 1000

type message struct {
	msg  string
	done chan struct{}
}

func randomSleep() {
	rand.Seed(time.Now().UnixNano())
	randDur := time.Duration(rand.Intn(maxRandNum)) * time.Millisecond
	time.Sleep(randDur)
	return
}

func randomNum(x int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(x)
}

func sender(done chan struct{}, m string) chan string {
	var c = make(chan string)
	mx := randomNum(maxRandNum)
	go func() {
		defer close(c)
		for i := 1; i <= mx; i++ {
			randomSleep()
			select {
			case c <- fmt.Sprintf("[#%v %s]", i, m):
			case <-done:
				return
			}
		}
	}()
	return c
}

func merge(done <-chan struct{}, cs ...<-chan string) <-chan string {
	var wg sync.WaitGroup
	out := make(chan string)

	// Start an output goroutine for each input channel in cs.  output
	// copies values from c to out until c or done is closed, then calls
	// wg.Done.
	output := func(c <-chan string) {
		defer wg.Done()
		for n := range c {
			select {
			case out <- n:
			case <-done:
				return
			}
		}
	}
	wg.Add(len(cs))

	// Fanin
	for _, c := range cs {
		go output(c)
	}

	// Start a goroutine to close out once all the output goroutines are
	// done.  This must start after the wg.Add call.
	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}

func main() {
	// Set up a done channel that's shared by the whole pipeline,
	// and close that channel when this pipeline exits, as a signal
	// for all the goroutines we started to exit.
	done := make(chan struct{})
	defer close(done)

	hello := sender(done, "hello")
	world := sender(done, "world")
	// Consume the first value from output.
	out := merge(done, hello, world)
	for i := 1; i < 50; i++ {
		fmt.Println(<-out) // 4 or 9
	}
	// done will be closed by the deferred call.
}
