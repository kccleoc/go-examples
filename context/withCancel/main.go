package main

import (
	"context"
	"fmt"
	"log"
	"time"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 6*time.Second)
	//terminated signal propagated to all routines
	//if using defer cancel() this would cancel all routine after time specified above
	cancel()
	waitTalker1(ctx, 7*time.Second, "ok google1")
	waitTalker2(ctx, 6*time.Second, "ok google2")
}

func waitTalker1(ctx context.Context, d time.Duration, msg string) {
	select {
	case <-time.After(d):
		fmt.Println(msg, " after ", d)

	case <-ctx.Done():
		fmt.Print("waitTalker1 stopped,")
		log.Println(ctx.Err())
	}
}

func waitTalker2(ctx context.Context, d time.Duration, msg string) {
	select {
	case <-time.After(d):
		fmt.Println(msg, " after ", d)

	case <-ctx.Done():
		fmt.Print("waitTalker2 stopped,")
		log.Println(ctx.Err())
	}
}
