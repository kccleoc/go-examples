package main

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"time"
)

var (
	// If os interrupt signal sent to unbuffered channel
	// it would drop without blocking so receiver wont be
	// notified if there is any system signal being invoked
	// To get around it, use buffer of 1
	sigChan = make(chan os.Signal, 1)
)

func random(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}

func main() {
	// By hitting ctrl-c, sigChan received os interrupt signal
	signal.Notify(sigChan, os.Interrupt)

	ctx, cancel := context.WithCancel(context.Background())

	// launch a go routine to do some works with cancellable ctx
	for x := 1; x <= 10; x++ {
		fmt.Printf("worker %v is working ~\n", x)
		go aWorker(x, ctx)
	}

	// go launch a stopper func who will send signal to stop all api with ctx
	// cancel is like a stopper. If you trigger ite, very routines with ctx
	// will stopped accordingly
	select {

	// all go rountines with ctx will stop after 10s
	case <-time.After(10 * time.Second):
		fmt.Printf("time out triggers ctx to cancal\n")
		cancel()
	// all go routines with ctx will stop when user hitting ctrl-c
	case <-sigChan:
		fmt.Printf("User Interrupt and triggers ctx to cancel\n")
		cancel()
	}

	time.Sleep(2 * time.Second)

}

func aWorker(workerNum int, ctx context.Context) {

	timeReq := random(1, 5)
	// simulate that a worker spent some seconds to perform a task
	time.Sleep(time.Duration(timeReq) * time.Second)
	fmt.Printf("worker %v spent %v sec in a task\n", workerNum, timeReq)

	// go rountine is blocked here to wait for signal to receive
	select {
	case <-ctx.Done():
		fmt.Printf("worker %v stopped, %s\n", workerNum, ctx.Err())
		return
	}
}
