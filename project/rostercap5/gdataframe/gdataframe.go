package gdataframe

import (
	"fmt"
	"strings"

	"github.com/kniren/gota/dataframe"
	"github.com/kniren/gota/series"
)

//DfStaffByMth prints out dataframe of of roster
//filter by mth and staff
func DfStaffByMth(sf string, csv string) {
	// import to dataframe and do sorting
	df := dataframe.ReadCSV(strings.NewReader(csv))
	df = df.Arrange(
		dataframe.Sort("Date"),
	)
	// apply filter , use Equal to
	fil := df.Filter(
		dataframe.F{
			Colname:    "Staff",
			Comparator: series.Eq,
			Comparando: sf,
		},
	)

	fmt.Println(fil)
}
