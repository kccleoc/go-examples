package xls2csv

import (
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"github.com/naoina/toml"
	"github.com/tealeg/xlsx"
)

// Xls is to store the roster xls file attrib.
type xls struct {
	SrcFile string
	TLeft   []int
	BRight  []int
}

// Shifts define the nature of shifts and API
type Shifts struct {
	WBook xls
	Yr    int
	Mth   int
	date  string
	staff string
	duty  string //e.g rds,mns,ds
}

// New return new Shifts struct by reading from toml location
func New(tomlFile string) *Shifts {
	var s Shifts
	//Read toml
	f, err := os.Open(tomlFile)
	if err != nil {
		fmt.Printf("could not open TOML file, %s", err)
	}
	defer f.Close()

	if err := toml.NewDecoder(f).Decode(&s.WBook); err != nil {
		fmt.Printf("could not unmarshal toml , %s", err)
	}
	return &s
}

// CsvExport method convert the pre-defined roster range 
// of xls to csv string
func (s *Shifts) ConvertCSV (fileName string) string {
	//first column heads
	csvString := fmt.Sprintf("Year,Month,Date,Staff,Duty\n")

	xlf, err := xlsx.OpenFile(s.WBook.SrcFile)
	if err != nil {
		fmt.Printf("could not open xlsx file, %s", err)
	}

	// make the Sheet Name according to convention
	shName := func() string {
		t := time.Date(s.Yr, time.Month(s.Mth), 1, 0, 0, 0, 0, time.UTC)
		return fmt.Sprint(t.Format("Jan 06"))
	}()

	for r := s.WBook.TLeft[0]; r <= s.WBook.BRight[0]; r++ {

		//get date cell ; default column "C"
		cv, _ := xlf.Sheet[shName].Rows[r].Cells[2].FormattedValue()
		if cv != "" {

			// format date to, eg., "Sep-25"
			dv := fmt.Sprintf("%s", cv[0:2]+"-"+cv[len(cv)-3:len(cv)])
			s.date = fmt.Sprintf("%s", cv[0:2])
			//dv[3:6] = Sep, sh[0:2] = SEP
			//get shift if date match the current month
			if dv[3:6] == shName[0:3] {
				for c := s.WBook.TLeft[1]; c <= s.WBook.BRight[1]; c++ {

					// add header value by refer to row 5 every time
					// if hv is empty , i.e. not a staff name, then maybe
					// it should "continue" the for loop
					hv := fmt.Sprintf("%s", xlf.Sheet[shName].Rows[5].Cells[c-1])
					// sometime sv might not be a shift
					sv := fmt.Sprintf("%s", xlf.Sheet[shName].Rows[r].Cells[c-1])
					if len(sv) < 1 {
						s.staff = hv
						s.duty = "wht"
					} else {
						s.staff = hv
						s.duty = sv
					}
					//csv
					csvString += fmt.Sprintf("%v,%v,%s,%s,%s\n", s.Yr, s.Mth, s.date, s.staff, s.duty)
				}
			}
		}
	}

	// output as .csv file if filename is provieded
	if fileName == "" {
		return csvString
	} else {
		err := ioutil.WriteFile(fileName, []byte(csvString), 0644)
		if err != nil {
			fmt.Printf("could not write to file %s ,%s", fileName, err)
			return ""
		}
		return ""
	}
}
