package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {

	//setup api request
	remotePostFile := "roster-199707.csv"
	localSrcFile := "test.csv"
	apiURL := "https://api.bitbucket.org/2.0/repositories/kccleoc/test-public/src"
	userName := "kccleoc"
	passwd := os.Getenv("BBTOKEN")
	fd, err := ioutil.ReadFile(localSrcFile)
	if err != nil {
		fmt.Printf("could not open file, %s", err) // handle err
		return
	}
	playload := strings.NewReader(remotePostFile + "=" + string(fd))

	//http - POST
	req, err := http.NewRequest("POST", apiURL, playload)
	if err != nil {
		fmt.Printf("could not make new request, %s", err) // handle err
	}
	req.SetBasicAuth(userName, passwd)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("could not get response, %s", err) // handle err
	}

	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	if len(string(responseData)) == 0 {
		fmt.Println("successfully posted!")
	}
	defer resp.Body.Close()
}
