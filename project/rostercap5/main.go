package main

import (
	"flag"
	"fmt"

	"bitbucket.org/kccleoc/go-examples/project-roster/rostercap5/gdataframe"
	"bitbucket.org/kccleoc/go-examples/project-roster/rostercap5/xls2csv"
)

const configTOML = "config.toml" //fixed location

func main() {
	var yr = flag.Int("y", 2017, "e.g. for 2017, -year=2017 or -year 2017")
	var mth = flag.Int("m", 7, "e.g. for Jul, -month=7 or -month 7")
	var csvfile = flag.String("f", "", "output as csv, e.g. -csvfile=")
	flag.Parse()
	staffing := xls2csv.New(configTOML)
	staffing.Yr = *yr
	staffing.Mth = *mth
	// this copy of csv is bad to usage of memory
	csvString := staffing.ConvertCSV(*csvfile)

	// function 1 -- default when not filename is given
	// preview the roster - TODO : add filter in API
	if *csvfile == "" {
		gdataframe.DfStaffByMth("Leo", csvString)
	}

	// function 2 -- output as csv file
	if *csvfile != "" {
		fmt.Printf("csv file saved as %s", *csvfile)
	}
}
