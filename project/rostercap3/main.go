package main

import (
	"fmt"
	"strings"

	"github.com/kccleoc/go-examples/project-roster/rostercap3/roster"
	"github.com/kniren/gota/dataframe"
	"github.com/kniren/gota/series"
)

func main() {
	//	Configuration
	srcFile := "./roster-sample.xlsm"
	yr, mth := 2016, 12
	tLight := [2]int{6, 7}
	bRight := [2]int{40, 20}
	staffHead := "Leo"

	// struct Initialised and fill it with shift data
	rtr := roster.New(yr, mth, srcFile, tLight, bRight)
	rtr.FillStruct()

	//export csv for using dataframe package
	csvString := rtr.TurnCSV()
	// import to dataframe and do sorting
	df := dataframe.ReadCSV(strings.NewReader(csvString))
	df = df.Arrange(
		dataframe.Sort("Date"),
	)
	// apply filter , use Equal to
	fil := df.Filter(
		dataframe.F{
			Colname:    "Staff",
			Comparator: series.Eq,
			Comparando: staffHead,
		},
	)

	fmt.Println(fil)
}
