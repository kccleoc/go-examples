package roster

import (
	"fmt"
	"time"

	"github.com/tealeg/xlsx"
)

type duty struct {
	worker string
	date   string
}

// define excel area by two corner point
type areaRef struct {
	topLeft  [2]int //row , column
	btmRight [2]int
}

// Roster is to store the roster attrib.
type Roster struct {
	shifts    map[duty]string
	srcPath   string
	sheetName string
	area      areaRef
}

// New to initialised a Roster struct
func New(y, m int, p string, t, b [2]int) *Roster {
	var rt Roster
	rt.shifts = make(map[duty]string)
	rt.getSheetName(y, m)
	rt.getSrcPath(p)
	rt.getAreaPoint(t, b)
	return &rt
}

// getSheetName contructs the name of sheet according to
// our naming convention like "Jan 15"
func (rt *Roster) getSheetName(y, m int) *Roster {
	t := time.Date(y, time.Month(m), 1, 0, 0, 0, 0, time.UTC)
	rt.sheetName = fmt.Sprint(t.Format("Jan 06"))
	return rt
}

// getSrcPath put excel source file path to struct Roster
func (rt *Roster) getSrcPath(p string) *Roster {
	rt.srcPath = p
	return rt
}

// getSrcPath put excel source file path to struct Roster
func (rt *Roster) getAreaPoint(t, b [2]int) *Roster {
	rt.area.topLeft = t
	rt.area.btmRight = b
	return rt
}

// FillStruct method perform data reading from excel to struct
func (rt *Roster) FillStruct() {
	xlf, err := xlsx.OpenFile(rt.srcPath)
	if err != nil {
		panic("error opening")
	}

	for r := rt.area.topLeft[0]; r <= rt.area.btmRight[0]; r++ {

		//get date cell ; default column "C"
		cv, _ := xlf.Sheet[rt.sheetName].Rows[r].Cells[2].FormattedValue()
		if cv != "" {

			// format date to, eg., "Sep-25"
			dv := fmt.Sprintf("%s", cv[0:2]+"-"+cv[len(cv)-3:len(cv)])

			//dv[3:6] = Sep, sh[0:2] = SEP
			//get shift if date match the current month
			if dv[3:6] == rt.sheetName[0:3] {
				for c := rt.area.topLeft[1]; c <= rt.area.btmRight[1]; c++ {

					// add header value by refer to row 5 every time
					// if hv is empty , i.e. not a staff name, then maybe
					// it should "continue" the for loop
					hv := fmt.Sprintf("%s", xlf.Sheet[rt.sheetName].Rows[5].Cells[c-1])
					// sometime sv might not be a shift
					sv := fmt.Sprintf("%s", xlf.Sheet[rt.sheetName].Rows[r].Cells[c-1])
					if len(sv) < 1 {
						rt.shifts[duty{hv, dv}] = "wht"
					} else {
						rt.shifts[duty{hv, dv}] = sv
					}

				}
			}

		}

	}
}

// TurnCSV export the Roster struct into CSV format
func (rt *Roster) TurnCSV() (csvStg string) {
	csvStg = fmt.Sprintf("Staff,Date,Duty\n")
	for k, v := range rt.shifts {
		csvStg += fmt.Sprintf("%s,%s,%s\n", k.worker, k.date, v)
	}
	return
}
