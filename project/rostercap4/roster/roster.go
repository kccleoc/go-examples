package roster

import (
	"fmt"
	"os"
	"time"

	"github.com/naoina/toml"
	"github.com/tealeg/xlsx"
)

type duty struct {
	worker string
	date   string
}

type shift map[duty]string

// Roster is to store the roster attrib.
type RosterConfig struct {
	SrcFile   string
	Yr        int
	Mth       int
	TLeft     []int
	BRight    []int
	Staffhead string
}

type RosterShifts struct {
	Rcfg      RosterConfig
	shifts    shift
	sheetName string
}

// readRosterConfig read from TOML
func readRosterConfig(rc *RosterConfig, t string) error {
	f, err := os.Open(t)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	if err := toml.NewDecoder(f).Decode(&rc); err != nil {
		return err
	}

	return nil
}

func NewRoster(tomlFile string) *RosterShifts {
	var rs RosterShifts
	err := readRosterConfig(&rs.Rcfg, tomlFile)
	if err != nil {
		panic(err)
	}
	rs.shifts = make(map[duty]string)

	// contructs the name of sheet according to
	// our naming convention like "Jan 15"
	t := time.Date(rs.Rcfg.Yr, time.Month(rs.Rcfg.Mth), 1, 0, 0, 0, 0, time.UTC)
	rs.sheetName = fmt.Sprint(t.Format("Jan 06"))
	return &rs
}

// FillStruct method perform data reading from excel to struct
func (rt *RosterShifts) FillStruct() {
	xlf, err := xlsx.OpenFile(rt.Rcfg.SrcFile)
	if err != nil {
		panic("error opening")
	}

	for r := rt.Rcfg.TLeft[0]; r <= rt.Rcfg.BRight[0]; r++ {

		//get date cell ; default column "C"
		cv, _ := xlf.Sheet[rt.sheetName].Rows[r].Cells[2].FormattedValue()
		if cv != "" {

			// format date to, eg., "Sep-25"
			dv := fmt.Sprintf("%s", cv[0:2]+"-"+cv[len(cv)-3:len(cv)])

			//dv[3:6] = Sep, sh[0:2] = SEP
			//get shift if date match the current month
			if dv[3:6] == rt.sheetName[0:3] {
				for c := rt.Rcfg.TLeft[1]; c <= rt.Rcfg.BRight[1]; c++ {

					// add header value by refer to row 5 every time
					// if hv is empty , i.e. not a staff name, then maybe
					// it should "continue" the for loop
					hv := fmt.Sprintf("%s", xlf.Sheet[rt.sheetName].Rows[5].Cells[c-1])
					// sometime sv might not be a shift
					sv := fmt.Sprintf("%s", xlf.Sheet[rt.sheetName].Rows[r].Cells[c-1])
					if len(sv) < 1 {
						rt.shifts[duty{hv, dv}] = "wht"
					} else {
						rt.shifts[duty{hv, dv}] = sv
					}

				}
			}

		}

	}
}

// TurnCSV export the Roster struct into CSV format
func (rt *RosterShifts) TurnCSV() string {
	csvString := fmt.Sprintf("Staff,Date,Duty\n")
	for k, v := range rt.shifts {
		csvString += fmt.Sprintf("%s,%s,%s\n", k.worker, k.date, v)
	}
	return csvString
}
