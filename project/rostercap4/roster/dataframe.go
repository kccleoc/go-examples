package roster

import (
	"fmt"
	"strings"

	"github.com/kniren/gota/dataframe"
	"github.com/kniren/gota/series"
)

func DfStaffByMth(rt *RosterShifts, csv string) {
	// import to dataframe and do sorting
	df := dataframe.ReadCSV(strings.NewReader(csv))
	df = df.Arrange(
		dataframe.Sort("Date"),
	)
	// apply filter , use Equal to
	fil := df.Filter(
		dataframe.F{
			Colname:    "Staff",
			Comparator: series.Eq,
			Comparando: rt.Rcfg.Staffhead,
		},
	)

	fmt.Println(fil)
}
