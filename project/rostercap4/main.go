package main

import (
	"github.com/kccleoc/go-examples/project-roster/rostercap4/roster"
)

const configTOML = "config.toml" //fixed location

func main() {
	r := roster.NewRoster(configTOML)
	r.FillStruct()
	csv := r.TurnCSV()
	roster.DfStaffByMth(r, csv)
}
