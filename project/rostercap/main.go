package main

import (
	"fmt"
	"time"

	"github.com/tealeg/xlsx"
)

func readCell(f *xlsx.File, tabName string, rRef int, cRef int) {
	fmt.Println(f.Sheet[tabName].Rows[rRef-1].Cells[cRef-1])
}

func readArea(f *xlsx.File, tabName string, rRef1 int, cRef1 int, rRef2 int, cRef2 int, fmd int) {
	//print a blank line
	var printline func()
	printline = func() {
		for c := cRef1; c <= cRef2+1; c++ {
			fmt.Printf("|%-8s", "-------")
		}
		fmt.Println("")
	}

	//Print Head
	fmt.Printf("|%-8s", "Date") //date
	for c := cRef1; c <= cRef2; c++ {
		fv, _ := f.Sheet[tabName].Rows[5].Cells[c-1].FormattedValue()
		fmt.Printf("|%-8s", fv)
	}
	fmt.Println("")
	printline()
	// end Print Head

	// 	blankRow := true
	validDate := 0

	for r := rRef1; r <= rRef2; r++ {

		//Print 1 column -- date
		cv, _ := f.Sheet[tabName].Rows[r].Cells[2].FormattedValue()
		if cv != "" {
			fmt.Printf("|%-8s", cv[0:2]+"-"+cv[len(cv)-3:len(cv)])
			validDate++

			//print shift
			for c := cRef1; c <= cRef2; c++ {
				cv := f.Sheet[tabName].Rows[r].Cells[c-1]
				fmt.Printf("|%-8s", cv)
			}

			//draw a line every 7 day according to weekday, to be implement
			fmt.Println("")
			if (validDate+(8-fmd))%7 == 0 {
				printline()
			}
		}

	}
}

func main() {
	//	Configuration
	excelFileName := "./roster-sample.xlsm"
	yr, mth := 2017, 3

	//Date calculation
	t := time.Date(yr, time.Month(mth), 1, 0, 0, 0, 0, time.UTC)
	sName := fmt.Sprint(t.Format("Jan 06"))
	fmd := (8-int(t.Weekday()))%7 + 1

	//open roster excel file
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		panic("error opening")
	}

	//Print full roster for the month
	readArea(xlFile, sName, 6, 7, 40, 20, fmd)

}
