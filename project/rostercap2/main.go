package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/kniren/gota/dataframe"
	"github.com/kniren/gota/series"
	"github.com/tealeg/xlsx"
)

type duty struct {
	worker string
	date   string
}

type roster struct {
	mth    string
	shifts map[duty]string
}

type areaRef struct {
	topLeft  [2]int //row , column
	btmRight [2]int
}

func getSheetName(y, m int) string {
	t := time.Date(y, time.Month(m), 1, 0, 0, 0, 0, time.UTC)
	return fmt.Sprint(t.Format("Jan 06"))
}

func (rt *roster) fillStruct(src string, sh string, ar areaRef) {
	xlf, err := xlsx.OpenFile(src)
	if err != nil {
		panic("error opening")
	}

	// this is the tab name of target roster
	rt.mth = sh

	for r := ar.topLeft[0]; r <= ar.btmRight[0]; r++ {

		//get date cell ; default column "C"
		cv, _ := xlf.Sheet[sh].Rows[r].Cells[2].FormattedValue()
		if cv != "" {

			// format date to, eg., "Sep-25"
			dv := fmt.Sprintf("%s", cv[0:2]+"-"+cv[len(cv)-3:len(cv)])

			//dv[3:6] = Sep, sh[0:2] = SEP
			//get shift if date match the current month
			if dv[3:6] == sh[0:3] {
				for c := ar.topLeft[1]; c <= ar.btmRight[1]; c++ {

					// add header value by refer to row 5 every time
					// if hv is empty , i.e. not a staff name, then maybe
					// it should "continue" the for loop
					hv := fmt.Sprintf("%s", xlf.Sheet[sh].Rows[5].Cells[c-1])
					// sometime sv might not be a shift
					sv := fmt.Sprintf("%s", xlf.Sheet[sh].Rows[r].Cells[c-1])
					if len(sv) < 1 {
						rt.shifts[duty{hv, dv}] = "wht"
					} else {
						rt.shifts[duty{hv, dv}] = sv
					}

				}
			}

		}

	}
}

func (rt *roster) turnCSV() (s string) {
	s = fmt.Sprintf("Staff,Date,Duty\n")
	for k, v := range rt.shifts {
		s += fmt.Sprintf("%s,%s,%s\n", k.worker, k.date, v)
	}
	return
}

func main() {

	//	Configuration
	srcFile := "./roster-sample.xlsm"
	yr, mth := 2016, 12
	sName := getSheetName(yr, mth)
	aRef := areaRef{
		topLeft:  [2]int{6, 7},
		btmRight: [2]int{40, 20},
	}

	// a month roster
	rt := &roster{
		mth:    "",
		shifts: make(map[duty]string),
	}

	rt.fillStruct(srcFile, sName, aRef)
	csvString := rt.turnCSV()
	// using dataframe package
	df := dataframe.ReadCSV(strings.NewReader(csvString))
	df = df.Arrange(
		dataframe.Sort("Date"),
		dataframe.RevSort("Staff"),
	)
	fil := df.Filter(
		dataframe.F{
			Colname:    "Staff",
			Comparator: series.Eq,
			Comparando: "Leo",
		},
	)

	fmt.Println(fil)
}
