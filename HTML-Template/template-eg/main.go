package main

import (
	"html/template"
	"log"
	"os"
)

// Page is a struct use to test template using struct
type Page struct {
	T string
	B string
}

func main() {
	var err error
	tpl := template.New("tpl.gohtml")
	tpl = tpl.Funcs(template.FuncMap{
		"justsayhello": func() string {
			return "return string"
		},
	})
	tpl, err = tpl.ParseFiles("tpl.gohtml")
	if err != nil {
		log.Fatalln(err)
	}
	err = tpl.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln(err)
	}
}
