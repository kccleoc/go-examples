package main

import (
	"fmt"
	"os"
)

func main() {
	s, sep := "", ""
	for i, arg := range os.Args[1:] {
		fmt.Print(i)
		s += sep + arg
		sep = " "
	}
	fmt.Println(s)
	fmt.Println(os.Args[1:])
}
