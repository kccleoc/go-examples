package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

var flagPath = flag.String("path", "", "rename ITGW503@ to nothing")

func visit(path string, f os.FileInfo, err error) error {
	monthnum := map[string]string{"Sep": "09",
		"Oct": "10",
		"Nov": "11",
		"Dec": "12",
		"Jan": "01",
		"Feb": "02",
		"Mar": "03",
		"Apr": "04",
		"May": "05",
		"Jun": "06",
		"Jul": "07",
	}
	if name := f.Name(); strings.Contains(name, ".pdf") {
		dir := filepath.Dir(path)
		newname := string(name[7:9]) + monthnum[string(name[3:6])] + string(name[0:2]) + ".pdf"
		// 		strings.Replace(name, "ITGW503@", "", 1)
		newpath := filepath.Join(dir, newname)
		fmt.Printf("mv %q %q\n", path, newpath)
		os.Rename(path, newpath)
	}
	return nil
}

func main() {
	flag.Parse()
	if *flagPath == "" {
		flag.Usage()
		return
	}
	filepath.Walk(*flagPath, visit)
}
