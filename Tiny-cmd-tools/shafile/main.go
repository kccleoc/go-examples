package main

import (
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"os"
)

func getsha(filename string) ([]byte, error) {
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	h := sha256.New()
	h.Write(bs)
	return h.Sum(nil), nil
}

func main() {
	fs := os.Args[1:]
	for _, f := range fs {
		s, err := getsha(f)
		if err != nil {
			fmt.Printf("could not load the file %v \n", f)
			return
		}
		fmt.Printf("sha256: %x\n", s)
	}

}
