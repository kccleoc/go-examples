package main

import (
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"io"
)

func hash(bs []byte) []byte {
	h := sha256.New()
	h.Write(bs)
	return h.Sum(nil)
}

func Random(n int) ([]byte, error) {
	bs := make([]byte, n)
	_, err := io.ReadFull(rand.Reader, bs)
	return bs, err
}

func main() {
	bs, err := Random(16)
	if err != nil {
		fmt.Printf("courld not generate rand num %v\n", err)
	}
	hashsum := hash(bs)
	fmt.Printf("Data : %x\nHashsum: %x\n", bs, hashsum)
}
