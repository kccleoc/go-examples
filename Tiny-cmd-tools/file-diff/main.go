package main

import (
	"crypto/sha256"
	"fmt"
	"hash/crc32"
	"io/ioutil"
)

func getHash(filename string) (uint32, error) {
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		return 0, err
	}
	h := crc32.NewIEEE()
	h.Write(bs)
	return h.Sum32(), nil
}

func getsha(filename string) ([]byte, error) {
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	h := sha256.New()
	h.Write(bs)
	return h.Sum(nil), nil
}

func main() {
	h1, err := getHash("test1.txt")
	if err != nil {
		return
	}
	h2, err := getHash("test2.txt")
	if err != nil {
		return
	}
	fmt.Println(h1, h2, h1 == h2)

	sha1, err := getsha("test1.txt")
	if err != nil {
		return
	}
	sha2, err := getsha("test2.txt")
	if err != nil {
		return
	}
	fmt.Printf("sha1: %x, sha2: %x", sha1, sha2)
}
