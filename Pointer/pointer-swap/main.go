package main

import (
	"fmt"
)

func swap(a, b *int) {
	//it can't swap the value stored in the address
	//but it swaps the pointers ' addresses only
	//so this function can't actually swap two number
	//solution : *a , *b = *b , *a
	temp := a
	fmt.Println("inside func, a , b ", a, b)
	fmt.Println("value a , b", *a, *b)
	a = b
	b = temp
	fmt.Println("inside func, a , b ", a, b)
	fmt.Println("value a , b", *a, *b)

}

func main() {
	x, y := 1, 2
	fmt.Println("inside main , x , y ", &x, &y)
	fmt.Println("value x , y", x, y)

	swap(&x, &y)
	fmt.Println(x, y)
	fmt.Println("inside main , x , y ", &x, &y)
	fmt.Println("value x , y", x, y)

}
