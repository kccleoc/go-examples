package main

import (
	f "fmt"
)

type Point struct {
	X, Y float64
}

func (p *Point) ScaleBy(factor float64) {
	p.X *= factor
	p.Y *= factor
}

func main() {
	// #1 pass to *Point receier
	r := &Point{1, 2}
	r.ScaleBy(2)
	f.Println(*r)
	// or #2
	p := Point{1, 2}
	ppointer := &p
	ppointer.ScaleBy(2)
	f.Println(p)
	// or #3
	f.Println(p)
	xx := new(Point)
	f.Println(&xx, *xx, xx)
}
