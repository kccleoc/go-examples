package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type HDC_Odds struct {
	XMLName xml.Name `xml:"SB_INFO"`
	Matches []Match  `xml:"MATCH"`
}

type Match struct {
	ID            string `xml:"ID,attr"`
	MATCH_STARTED string `xml:"MATCH_STARTED,attr"`
	MATCH_STAGE   string `xml:"MATCH_STAGE,attr"`
	STATUS        string `xml:"STATUS,attr"`
	VOID          string `xml:"VOID,attr"`
	HASRESULT     string `xml:"HASRESULT,attr"`
	INPLAY_POOLS  string `xml:"INPLAY_POOLS,attr"`
	HT_POOLS      string `xml:"HT_POOLS,attr"`
	Pools         []Pool `xml:"POOL"`
}

type Pool struct {
	TYPE              string `xml:",attr"`
	IN                string `xml:",attr"`
	MATCH_POOL_STATUS string `xml:",attr"`
	INPLAY            bool   `xml:",attr"`
	HT_SELL           bool   `xml:",attr"`
	SELL              bool   `xml:",attr"`
	ISINITIAL_ODDS    bool   `xml:",attr"`

	// HAD
	A string `xml:",attr"`
	D string `xml:",attr"`
	H string `xml:",attr"`

	// CRS
	M1MA  string `xml:",attr"`
	M1MD  string `xml:",attr"`
	M1MH  string `xml:",attr"`
	S0000 string `xml:",attr"`
	S0001 string `xml:",attr"`
	S0002 string `xml:",attr"`
	S0003 string `xml:",attr"`
	S0004 string `xml:",attr"`
	S0005 string `xml:",attr"`
	S0100 string `xml:",attr"`
	S0101 string `xml:",attr"`
	S0102 string `xml:",attr"`
	S0103 string `xml:",attr"`
	S0104 string `xml:",attr"`
	S0105 string `xml:",attr"`
	S0200 string `xml:",attr"`
	S0201 string `xml:",attr"`
	S0202 string `xml:",attr"`
	S0203 string `xml:",attr"`
	S0204 string `xml:",attr"`
	S0205 string `xml:",attr"`
	S0300 string `xml:",attr"`
	S0301 string `xml:",attr"`
	S0302 string `xml:",attr"`
	S0303 string `xml:",attr"`
	S0400 string `xml:",attr"`
	S0401 string `xml:",attr"`
	S0402 string `xml:",attr"`
	S0500 string `xml:",attr"`
	S0501 string `xml:",attr"`
	S0502 string `xml:",attr"`

	// HFT
	AA string `xml:",attr"`
	AD string `xml:",attr"`
	AH string `xml:",attr"`
	DA string `xml:",attr"`
	DD string `xml:",attr"`
	DH string `xml:",attr"`
	HA string `xml:",attr"`
	HD string `xml:",attr"`
	HH string `xml:",attr"`

	// TTG
	P0 string `xml:",attr"`
	P1 string `xml:",attr"`
	P2 string `xml:",attr"`
	P3 string `xml:",attr"`
	P4 string `xml:",attr"`
	P5 string `xml:",attr"`
	P6 string `xml:",attr"`
	M7 string `xml:",attr"`

	// HHA line
	HG string `xml:",attr"`
	AG string `xml:",attr"`

	// OOE
	E string `xml:",attr"`
	O string `xml:",attr"`

	// HIL
	L             string `xml:",attr"`
	LINE          string `xml:",attr"`
	LINE_STATUS   string `xml:",attr"`
	LINE_NO       string `xml:",attr"`
	MAINLINE_FLAG string `xml:",attr"`

	// FGS
	S000 string `xml:",attr"`
	S101 string `xml:",attr"`
	S102 string `xml:",attr"`
	S103 string `xml:",attr"`
	S104 string `xml:",attr"`
	S105 string `xml:",attr"`
	S106 string `xml:",attr"`
	S107 string `xml:",attr"`
	S108 string `xml:",attr"`
	S109 string `xml:",attr"`
	S110 string `xml:",attr"`
	S111 string `xml:",attr"`
	S112 string `xml:",attr"`
	S113 string `xml:",attr"`
	S114 string `xml:",attr"`
	S115 string `xml:",attr"`
	S116 string `xml:",attr"`
	S117 string `xml:",attr"`
	S118 string `xml:",attr"`
	S119 string `xml:",attr"`
	S120 string `xml:",attr"`
	S201 string `xml:",attr"`
	S202 string `xml:",attr"`
	S203 string `xml:",attr"`
	S204 string `xml:",attr"`
	S205 string `xml:",attr"`
	S206 string `xml:",attr"`
	S207 string `xml:",attr"`
	S208 string `xml:",attr"`
	S209 string `xml:",attr"`
	S210 string `xml:",attr"`
	S211 string `xml:",attr"`
	S212 string `xml:",attr"`
	S213 string `xml:",attr"`
	S214 string `xml:",attr"`
	S215 string `xml:",attr"`
	S216 string `xml:",attr"`
	S217 string `xml:",attr"`
	S218 string `xml:",attr"`
	S219 string `xml:",attr"`
	S220 string `xml:",attr"`

	// FTS
	N string `xml:",attr"`

	// SPC
	S01 string `xml:",attr"`
	S02 string `xml:",attr"`
	S03 string `xml:",attr"`
	S04 string `xml:",attr"`
	S05 string `xml:",attr"`
}

func oddsScrape() {
	resp, err := http.Get("http://bet.hkjc.com/football/getXML.aspx?pooltype=all")
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	respHDC, err := ioutil.ReadAll(resp.Body)

	hdcOdds := HDC_Odds{}
	err = xml.Unmarshal(respHDC, &hdcOdds)
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}

	fmt.Printf("%+v ", hdcOdds.Matches[2])

}

func getID() {
	resp, err := http.Get("http://bet.hkjc.com/football/getXML.aspx?pooltype=HDC")
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	respID, err := ioutil.ReadAll(resp.Body)
	fmt.Println(respID)
}

func main() {
	oddsScrape()
}
