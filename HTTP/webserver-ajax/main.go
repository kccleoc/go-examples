package main

import (
	"html/template"
	"log"
	"net/http"
)

type page struct {
	Title string
	Body  []byte
}

func handlePage(w http.ResponseWriter, r *http.Request) {
	rootPage := &page{Title: "Test Landing Page", Body: []byte("This is a sample page")}
	t, _ := template.ParseFiles("index.go.html")
	t.Execute(w, rootPage)

}

func main() {
	http.HandleFunc("/", handlePage)
	http.Handle("/tmpfiles/", http.StripPrefix("/tmpfiles/", http.FileServer(http.Dir("./tmp"))))
	log.Fatal(http.ListenAndServe(":2002", nil))
}
