package main

import (
	"fmt"

	"github.com/asciimoo/colly"
)

func main() {
	c := colly.NewCollector()

	c.AllowedDomains = []string{"hk.apple.nextmedia.com", "www.nextdigital.com.hk"}

	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		fmt.Printf("Link found: %q -> %s\n", e.Text, link)
		c.Visit(e.Request.AbsoluteURL(link))
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit("http://hk.apple.nextmedia.com/archive/index/20171007/index/")
}
