package main

import (
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/smtp"
	"os"
	"time"
)

type mailCfg struct {
	mailAC     string
	appPwd     string
	smtpServer string
}

// 	err := smtp.SendMail("smtp.gmail.com:587",
// 		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
// 		from, []string{to}, []byte(msg))

func getsha(bs []byte) string {
	h := sha256.New()
	h.Write(bs)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func httpGet(url string) []byte {
	var netTransport = &http.Transport{
		Dial:                (&net.Dialer{Timeout: 5 * time.Second}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
		MaxIdleConns:        10,
		IdleConnTimeout:     30 * time.Second,
		//DisableCompression:  true,
	}
	var netClient = &http.Client{
		Timeout:   time.Second * 10,
		Transport: netTransport,
	}
	resp, err := netClient.Get(url)
	if err != nil {
		fmt.Printf("could not perform Get request, %s", err)
		return nil
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("could not read respose, %s", err)
		return nil
	}
	return body
}

func sendGMail(mc *mailCfg, to, body string) {

	msg := "From: " + mc.mailAC + "\n" +
		"To: " + to + "\n" +
		"Subject: " + body

	err := smtp.SendMail(mc.smtpServer,
		smtp.PlainAuth("", mc.mailAC, mc.appPwd, "smtp.gmail.com"),
		mc.mailAC, []string{to}, []byte(msg))

	if err != nil {
		log.Printf("smtp error: %s", err)
		return
	}

	log.Print("sent!")

}

func main() {
	url := "https://reserve-prime.apple.com/HK/en_HK/reserve/iPhoneX/availability?channel=1"
	// sha the url response
	respSHA := (getsha(httpGet(url)))

	// email notification
	gmail := func() {
		token := os.Getenv("LCYSGMAIL") // check environmental var
		mcfg := &mailCfg{
			mailAC:     "leochan@yuksing.com",
			appPwd:     token,
			smtpServer: "smtp.gmail.com:587",
		}

		mailBody := time.Now().Format("2006-01-02T15:04:05.999999-07:00") + "\n" + `
        iReserve open to access now
        Link: https://reserve-prime.apple.com/HK/en_HK/reserve/iPhoneX/availability?channel=1
        go check it
    `
		sendGMail(mcfg, "kccleoc@gmail.com", mailBody)
	}

	haveChange := func(s *string, u string) bool {
		newSHA := getsha(httpGet(u))
		//fmt.Print(*s)
		if *s != newSHA {
			*s = newSHA
			return true
		}
		return false
	}

	for {
		time.Sleep(10 * time.Second)
		if change := haveChange(&respSHA, url); change {
			gmail()
			continue
		}
		fmt.Print("x.")
	}

}
