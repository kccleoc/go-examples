package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	fromDay, toDay := os.Args[1], os.Args[2]
	parsefrom, _ := time.Parse("2006-01-02", fromDay)
	parseTo, _ := time.Parse("2006-01-02", toDay)
	dur := parseTo.Sub(parsefrom)
	fmt.Println(int(dur / (time.Hour * 24)))
}
