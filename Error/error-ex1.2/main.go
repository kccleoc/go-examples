// Using error structs and type checking

package main

import "fmt"

// define struct mathError to handle Maths error
type mathError struct {
	eMsg string
}

// function NewErr to jot a error msg to *mathError
func NewErr(m string) *mathError {
	return &mathError{eMsg: m}
}

// Impletment Error() string method in order to
// fulfil the signature of standard error interface
func (me mathError) Error() string {
	return me.eMsg
}

// divide is to divide two float numbers
func divide(a, b float64) (float64, error) {
	if b == 0.0 {
		return 0.0, NewErr(fmt.Sprintf("%v can't be divided it by zero", a))
	}

	return a / b, nil
}

func main() {
	result, err := divide(123, 0)
	if err != nil {
		switch e := err.(type) {
		case *mathError: //pointer semantics
			fmt.Println(e.eMsg)
		default:
			fmt.Println("other errors")
		}
	}
	fmt.Println(result)
}
