package main

import "fmt"

type PathError struct {
	Op   string
	Path string
	Err  error
}

type file struct {
	name     string
	nonblock bool // whether we set nonblocking mode
}

type File struct {
	*file
}

//*PathFile implement error
func (e *PathError) Error() string { return e.Op + " " + e.Path + ": ERROR " }

func OpenFile(name string) (*File, error) {
	return nil, &PathError{"open", name, nil}
}

func main() {
	f, err := OpenFile("xx.file")
	fmt.Println(f)
	fmt.Println(err)
}
