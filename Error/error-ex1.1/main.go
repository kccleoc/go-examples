package main

import "fmt"

type someError struct {
	eMsg string // description of error
}

func (e *someError) Error() string { return e.eMsg }

func isEven(i int) (bool, error) {
	se := someError{eMsg: "smell bad"}
	if i%2 == 0 {
		return true, nil
	} else {
		return false, &se
	}

}

func main() {
	fmt.Println(isEven(10))
}
