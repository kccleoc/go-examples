package main

import "fmt"

func multiple35(numLimit []int, m1, m2 int) int {
	var sum int = 0
	for i, _ := range numLimit {
		if (i%m1 == 0) || (i%m2 == 0) {
			numLimit[i] = i
			fmt.Println(numLimit)
			sum += i
		}

	}
	return sum
}

func main() {
	sl := make([]int, 1000)
	fmt.Println("start")
	fmt.Println(multiple35(sl, 3, 5))
}
