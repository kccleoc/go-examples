package main

import (
	"fmt"

	"github.com/kccleoc/GoLangCodes/geometry"
)

func main() {
	p := geometry.Point{1, 2}
	q := geometry.Point{4, 6}
	fmt.Println(geometry.Distance(p, q)) //function call
	fmt.Println(p.Distance(q))           //method call

	perim := geometry.Path{
		{1, 1},
		{5, 1},
		{5, 4},
		{1, 1},
	}

	fmt.Println(perim.Distance())
}
