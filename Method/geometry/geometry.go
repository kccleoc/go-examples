package geometry

import "math"

type Point struct{ X, Y float64 }

//Path is a journey connecting points with straight lines
type Path []Point

//Distance of Type Path returns the distance traveled along the path
func (pth Path) Distance() float64 {
	sum := 0.0
	for i := range pth {
		if i > 0 {
			sum += pth[i-1].Distance(pth[i])
		}
	}
	return sum
}

//traditional function
func Distance(p, q Point) float64 {
	return math.Hypot(q.X-p.X, q.Y-p.Y)
}

//same thing, but as a method of Point type
func (p Point) Distance(q Point) float64 {
	return math.Hypot(q.X-p.X, q.Y-p.Y)
}
